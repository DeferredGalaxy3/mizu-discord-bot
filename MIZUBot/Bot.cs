﻿using Discord;
using Discord.Audio;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIZUBot {
    class Bot {
        DiscordClient discordClient;

        public Bot (string token, string channel) {
            discordClient = new DiscordClient (x => {
                x.LogLevel = LogSeverity.Info;
                x.LogHandler = Log;
            });

            discordClient.UsingCommands (x => {
                x.PrefixChar = '!';
                x.AllowMentionPrefix = true;
            });

            var commands = discordClient.GetService<CommandService> ();
           
            commands.CreateCommand ("game")
                .Parameter ("Game", ParameterType.Required)
                .Do (async (e) => {
                    if (e.User.ServerPermissions.Administrator) {
                        await e.Channel.SendMessage ("Game set!");
                        discordClient.SetGame ($"{e.GetArg ("Game")}");
                    }
                });

            commands.CreateCommand ("say")
                .Parameter ("Words", ParameterType.Required)
                .Do (async (e) => {
                    if (e.User.ServerPermissions.Administrator) {
                        await e.Server.FindChannels (channel).FirstOrDefault ().SendMessage ($"{e.GetArg ("Words")}");
                    }
                });

            commands.CreateCommand ("aaaaaa")
                .Parameter ("Words", ParameterType.Required)
                .Do (async (e) => {
                    await e.User.SendMessage ($"{e.GetArg ("Words")}");
                });

            commands.CreateCommand ("match")
                .Parameter ("Person1", ParameterType.Required)
                .Parameter ("Person2", ParameterType.Required)
                .Do (async (e) => {
                    string personOne = $"{e.GetArg ("Person1")}";
                    string personTwo = $"{e.GetArg ("Person2")}";
                    await e.Server.FindChannels (channel).FirstOrDefault ().SendMessage (MatchMaking.Result (personOne, personTwo));
                });

            commands.CreateCommand ("idearandomizer")
                .Parameter ("Initiator", ParameterType.Optional)
                .Parameter ("Reciever", ParameterType.Optional)
                .Do (async (e) => {
                    await e.Server.FindChannels (channel).FirstOrDefault ().SendMessage ("Idea randomizer result: " + IdeaRandomizer.Randomize ($"{e.GetArg ("Initiator")}", $"{e.GetArg ("Reciever")}"));
                });

            discordClient.ExecuteAndWait (async () => {
                await discordClient.Connect (token, TokenType.Bot);
            });
        }


        private void Log (object sender, LogMessageEventArgs e) {
            Console.WriteLine (e.Message);
        }
        
    }
}