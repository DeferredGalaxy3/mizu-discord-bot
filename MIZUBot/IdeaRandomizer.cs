﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIZUBot {
    class IdeaRandomizer {

        public static Random random = new Random ();
        public static string [] textFile () {
            return System.IO.File.ReadAllLines (@"randomizerlist.txt");
        }

        public static List<string> getCategory (string categoryName) {
            List<string> testGetCategory = new List<string> ();
            bool collectNames = false;
            for (int i = 0; i < textFile ().Length; i++) {
                if (collectNames) {
                    if (textFile () [i].Length > 1) {
                        if (textFile () [i].Substring (0, 2) == "* ") {
                            testGetCategory.Add (textFile () [i].Substring (2));
                        }
                    } else {
                        collectNames = false;
                    }
                }
                if (textFile () [i] == "-- " + categoryName + " --") {
                    collectNames = true;
                }
            }
            return testGetCategory;
        }

        public static List<string> Actions () {
            return getCategory ("Actions");
        }

        public static List<string> Category (string name) {
            return getCategory (name);
        }

        public static bool IsOdd (int number) {
            while (number > 0) {
                number -= 2;
            }
            if (number < 0) {
                return true;
            } else {
                return false;
            }
        }

        public static string FullPhrase (string actionPhrase, string initiator, string reciever) {
            string [] halves = actionPhrase.Split ('(', ')');
            string fullPhrase = "";
            bool initiatorSet = false;
            bool recieverSet = false;
            for (int i = 0; i < halves.Length; i++) {
                if (IsOdd (i)) {
                    string [] categories = halves [i].Split ('|');
                    List<string> availableStrings = new List<string> ();
                    for (int x = 0; x < categories.Length; x++) {
                        for (int y = 0; y < Category (categories [x]).Count; y++) {
                            availableStrings.Add (Category (categories [x]) [y]);
                        }
                    }
                    halves [i] = categories [random.Next (categories.Length)];
                    if (initiator != "" && !initiatorSet && halves [i] == "Character Initiator") {
                        halves [i] = initiator;
                        initiatorSet = true;
                    } else if (reciever != "" && !recieverSet && halves [i] == "Character Reciever") {
                        halves [i] = reciever;
                        recieverSet = true;
                    } else {
                        halves [i] = availableStrings [random.Next (availableStrings.Count)];
                    }
                }
                fullPhrase = fullPhrase + halves [i];
            }
            return fullPhrase + ".";
        }

        public static string Randomize (string initiator, string reciever) {
            return FullPhrase (Actions () [random.Next (Actions ().Count)], initiator, reciever);
        }

        public static bool LineIsInFile (string name) {
            bool found = false;
            for (int i = 0; i < textFile ().Length; i++) {
                if (name == textFile () [i]) {
                    found = true;
                }
            }
            return found;
        }

        public static int Line (string name) {
            int line = 0;
            for (int i = 0; i < textFile ().Length; i++) {
                if (name == textFile () [i]) {
                    line = i;
                }
            }
            return line;
        }
    }
}
