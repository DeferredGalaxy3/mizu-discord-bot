﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIZUBot {
    public class MatchMaking {
        public static string Result (string person1, string person2) {
            Random random = new Random ();
            int number = random.Next (0, 101);
            string result = "Result: |";
            for (int i = 1; i <= 10; i++) {
                if (i == 1) {
                    if (number > 0) {
                        result = result + "♥";
                    }
                } else {
                    if (number >= i * 10) {
                        result = result + "♥";
                    } else {
                        result = result + "▯";
                    }
                }
            }
            result = result + "|\n\n";
            if (number == 100) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! You two look sooooooo cute together!~♥";
            } else if (number > 90) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! I want to be there for the wedding!";
            } else if (number > 80) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! At the very least, I see you two kissing!";
            } else if (number > 70) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! Nothing too special, but anything can work out!";
            } else if (number > 60) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! There may be bumps in the way, but with a little effort, they'll do just fine!";
            } else if (number > 50) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! It's a relationship.";
            } else if (number > 30) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! Um... Maybe the date will work out?";
            } else if (number > 10) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! Sorry... I don't think it's worth the effort.";
            } else if (number > 0) {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! Oh crap, this is a bad one.";
            } else {
                result = result + person1 + " and " + person2 + " are " + number.ToString () + "%! I uh... um... wow.";
            }
            return result;
        }
    }
}
