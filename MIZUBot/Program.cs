﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIZUBot {
    class Program {

        public static string [] textFile () {
            return System.IO.File.ReadAllLines (@"MIZUData.txt");
        }

        static void Main (string [] args) {
            try {
                string [] test = System.IO.File.ReadAllLines (@"MIZUData.txt");
                Console.WriteLine ("Loaded MIZU Data with token of " + test [0] + ".");
                Console.WriteLine ("Posting to channel #" + textFile () [1] + ".");
            } catch {
                Console.WriteLine ("No MIZUData.txt detected! You will now be prompted to enter bot data. This will write to the new MIZUData.txt file.");
                Console.WriteLine ("Enter the bot's token.");
                string token = Console.ReadLine ();
                Console.WriteLine ("Enter the channel your bot will be replying in. (Without the # pound sign.)");
                string channel = Console.ReadLine ();
                string [] newFile = { token, channel };
                System.IO.File.WriteAllLines (@"MIZUData.txt", newFile);
                Console.WriteLine ("Written new MIZUData.txt file to " + @"MIZUData.txt" + ".");
                Console.WriteLine ("Loaded MIZU Data with token of " + textFile () [0] + ".");
                Console.WriteLine ("Posting to channel #" + textFile () [1] + ".");
            }
            Bot mizu = new Bot (textFile () [0], textFile () [1]);
        }
    }
}
