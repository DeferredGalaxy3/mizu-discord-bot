# How to build.
1. Install the latest Visual Studio.
2. Go to Tools -> NuGet Package Manager -> Manage NuGet Packages for Solution...
3. Go to the Browse tab.
4. Search "Discord."
5. Install the following: Discord.Net, Discord.Net.Commands, Discord.Net.Audio, Discord.Net.Modules. (Do not check "Include prerelease." Install "Latest stable 0.9.6.")
6. You are now ready to run MIZU.
